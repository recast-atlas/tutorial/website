---
title: "Introducing Workflows"
teaching: 20
exercises: 30
questions:
- "What is the ultimate physics goal in interpreting our analysis of the VHbb signal model?"
- "What is the goal of RECAST in the context of interpreting our analysis?"
- "How does yadage help us preserve our analysis workflow for re-interpretation?"
objectives:
- "Understand the concept (but not necessarily the details!) of combining our signal model with data and SM background to interpret our analysis as a search for new physics."
- "Get a high-level overview of how yadage can help you preserve your analysis for re-interpretation."
keypoints:
- "Our end goal with the VHbb analysis is to perform a statistical comparison of our signal model with ATLAS data and SM backgrounds to determine whether we can see evidence of the signal in the data."
- "The goal of RECAST is to preserve and automate the procedure of re-interpreting our analysis with a new signal model."
- "Yadage helps you preserve each step of passing an arbitrary signal model through our analysis chain as a containerized process, and combine these steps into an automated workflow."
- "Try to avoid hard-coding anything to do with the signal model while developing your analysis, since this info will change when it's re-interpreted. Better yet, maintain your RECAST framework as you develop your analysis so you don't even have to think about it!"

---

## Interpreting our Analysis

So far in our VHbb analysis, we've taken a Monte Carlo simulated signal DAOD, looped through this MC data event-by-event, applied some kinematic selections, and output the data to a histogram of the dijet invariant mass. And this has now been preserved thanks to docker and GitLab, yay!  But what we really want to do in the end is compare this (presumably new) simulated signal sample with (1) data from the ATLAS detector and (2) our background estimate to determine whether we can see our signal in the data and/or constrain it using some sort of statistical analysis.  We are part of the way there, but not quite finished, since we only have an `mjj` histogram for our signal.

### The RECAST *Reinterpretation* Setup

The conventional RECAST setup that is used to perform reinterpretations is what we will be aiming for in this tutorial.  Our analysis has been done once, meaning that we have already gone through the painstaking work of running over the entire dataset and also performing a careful background estimation, ultimately arriving at a set of histograms that represent these two things : (1) **data** and (2) **background**.  For a reinterpretation, these will remain unchanged and can be "eaten" by a *parameterized* workflow as they are.

<img src="../fig/background_preservation.png" alt="Background preservation" style="width:350px">

Now what we want is to be able to produce a new interpretation and compare signal+background to data for our new signal (first image) as well as a statistical CLs interpretation for this particular signal (second image).

Spectrum of Data/MC        |  Reinterpreted Limit
:-------------------------:|:-------------------------:
<img src="../fig/results.png" alt="spectrum" style="width:350px">   |  <img src="../fig/limits.png" alt="spectrum" style="width:350px">


There are three primary additional components that we will need to use within our workflow :
  - **EOS** : For retrieving/storing the workflow inputs and outputs
  - **Post Processing** : For scaling of our signal
  - **Statistical Analysis** : For doing the final fit and interpretation

Let's spend some time getting familiar with these utilities and then we will begin to tie them together into a jazzy workflow using some new tools - [packtivity](https://github.com/yadage/packtivity), [yadage](https://github.com/yadage/yadage), and [recast](https://github.com/recast-hep/recast-atlas).

#### Getting our Data and Background (EOS)
As you have learned in the [HSF GitLab tutorial](https://hsf-training.github.io/hsf-training-cicd/), data is preserved and distributed on CERN GitLab via EOS using service accounts. As such, that is where our data and background samples are stored.  More specifically, they are stored at `root://eosuser.cern.ch//eos/user/r/recasttu/ATLASRecast2021/external_data.root` and within that file you will find two histograms `background` and `data` which were produced with the same `event-selection` code but running over the entirety of the Run 2 dataset and all MC campaigns ... (not actually, they were produced with [this code](https://gitlab.cern.ch/recast-atlas/tutorial/background-generation) but we are going to pretend for the sake of the tutorial).

#### Post Processing
Normally, between doing the event loop and the statistical analysis, you have some set of scripts for processing your histograms, right?  In particular, for scaling your raw histograms to physically meaningful luminosities and cross sections.  For our analysis chain, these utilities are bundled into a docker image `gitlab-registry.cern.ch/recast-atlas/tutorial/post-processing:master` housed at [this repo](https://gitlab.cern.ch/recast-atlas/tutorial/post-processing) though you should not need to look at the code to write your workflow, just know what the parameterized script eats and spits.

  > ## Exercise (15 min)
  > Practice running the python scripts that scale the signal to the correct cross section and luminosity from within the `gitlab-registry.cern.ch/recast-atlas/tutorial/post-processing:master-5173e8ae` image. For our purposes, the key utility we want is for scaling our signal appropriately.
  > ```bash
  > python scale_to_lumi_xsec.py -i selected.root -o output_scaled.root -p scaled.pdf -c 1 -s 1 -k 1 -f 1 -l 1 -g h_mjj
  > ```
  > the arguments are :
  >  - `-i`: The input file containing the histogram you want to scale
  >  - `-o`: The output file where you will write the scaled histogram
  >  - `-p`: The filename for making the debug plot
  >  - `-c`: cross section
  >  - `-s`: The sum of event weights
  >  - `-k`: k-factor
  >  - `-f`: filter efficiency
  >  - `-l`: luminosity
  >  - `-g`: The histogram in the file that is to be scaled
  >
  > `cd` out of the `event-selection-tutorial` repo and make a directory to contain the outputs of your scaling and statistical analysis:
  >  Assuming we're starting in the event-selection-tutorial repo:
  > ```bash
  > cd ..
  > mkdir scale-and-analyze
  > cd scale-and-analyze
  >```
  >
  > **HINT:** You'll need to source the `release_setup.sh` script (`source ~/release_setup.sh`) after you first start up the container with `docker run [...]` in order to set up the ATLAS software environment.
  >
  > > ## Solution
  > >
  > > Run the docker image `gitlab-registry.cern.ch/recast-atlas/tutorial/post-processing:master-5173e8ae` as a container, and volume mount the skimmed signal histogram `output_hist.root` that we created in the 'Toy Analysis' lesson from `event-selection-tutorial/run` into the container. Also volume mount the current working directory into a directory `/code/Tutorial` which will house the outputs of our scaling and statistical analysis, and source the atlas software environment:
  > > ```bash
  > > docker run --rm -it -v $PWD:/code/Tutorial -v $PWD/../event-selection-tutorial/run/output_hist.root:/Data/output_hist.root gitlab-registry.cern.ch/recast-atlas/tutorial/post-processing:master-5173e8ae
  > > ```
  > >```bash
  > > source ~/release_setup.sh
  > > ```
  > > Now, run `scale_to_lumi_xsec.py`, adapting the command-line arguments to run on the input file `/Data/output_hist.root` and produce an output `output_scaled.root` in the mounted `/code/Tutorial` directory.
  > >
  > > The command to produce an output file `output_scaled.root` under `/code/Tutorial` is:
  > > ```bash
  > > python scale_to_lumi_xsec.py -i /Data/output_hist.root -o /code/Tutorial/output_scaled.root -p scaled.pdf -c 1 -s 1 -k 1 -f 1 -l 1 -g h_mjj
  > > ```
  > > Then exit out of the container and do an `ls` to make sure that the file `output_scaled.root` has been produced:
  > > ```bash
  > > exit
  > > ls
  > > ```
  > > should output:
  > > ```
  > > output_scaled.root
  > > ```
  > >
  > {: .solution}
  >
  {: .challenge}

#### Statistical Analysis
After all inputs are prepared (background and data from EOS / signal scaled to cross section), we are ready to do the combined fit to extract the CLs value and know if we can exclude our signal.  There are a number of packages to do this, but here, we will be using a python fitting framework called [pyhf](https://diana-hep.org/pyhf/).  This has been bundled into a docker image `gitlab-registry.cern.ch/recast-atlas/tutorial/fitting:master` and if we embrace parameterized code, you shouldn't need to look at the code and your entire workflow can be written by knowing its interface (like all well-written code).

  > ## Exercise (15 min)
  > Practice running the python scripts that do the likelihood fit within the `gitlab-registry.cern.ch/recast-atlas/tutorial/fitting:master-3277bf68` image. For our purposes, the key utility we want is for scaling our signal appropriately.
  > The executable can be executed as
  > ```
  > python run_fit.py --filesig output_scaled.root --histsig h_mjj   --filebkg external_data.root --histbkg background  --filedata external_data.root --histdata data --outputfile limits.png --plotfile results.png
  > ```
  > with the input arguments being :
  >  - `--filesig`    : The path to the input file containing your signal histogram
  >  - `--histsig`    : The name of the signal histogram in that file
  >  - `--filebkg`    : The path to the input file containing your background histogram
  >  - `--histbkg`    : The name of the background histogram in that file
  >  - `--filedata`   : The path to the input file containing your data histogram
  >  - `--histdat`    : The name of the data histogram in that file
  >  - `--outputfile` : The png (or whatever) file name for saving your limits plot
  >  - `--plotfile`   : The png (or whatever) file name for saving your data/MC plot
  >
  > Run the docker image `gitlab-registry.cern.ch/recast-atlas/tutorial/fitting:master-3277bf68`, as a container, run kinit to set up kerboros authentication, then download the input file `external_data.root` into the container:
  > ```bash
  > docker run --rm -it -v $PWD:/code/Tutorial gitlab-registry.cern.ch/recast-atlas/tutorial/fitting:master-3277bf68
  > kinit recasttu@CERN.CH
  > # Enter recasttu password [It's the 'Password' in this snippet: https://gitlab.cern.ch/-/snippets/1553]
  > xrdcp root://eosuser.cern.ch//eos/user/r/recasttu/ATLASRecast2021/external_data.root .
  > ```
  > Now run `run_fit.py`, adapting the command-line arguments to run on the input file `/code/Tutorial/output_scaled.root` and produce the output `limits.png` and `results.png` in the mounted directory `/code/Tutorial`. Exit out of the container and check that the expected outputs have been produced in the current directory.
  >
  > > ## Solution
  > > The command to produce the output files in the mounted directory `/code/Tutorial` is:
  > > ```bash
  > > python run_fit.py --filesig /code/Tutorial/output_scaled.root --histsig h_mjj  --filebkg external_data.root --histbkg background  --filedata external_data.root --histdat data --outputfile /code/Tutorial/limits.png --plotfile /code/Tutorial/results.png
  > > ```
  > > Then exit out of the container and do an `ls` to make sure that the files `limits.png` and `results.png` have been produced:
  > > ```bash
  > > exit
  > > ls
  > > ```
  > > should output:
  > > ```
  > > limits.png  results.png  output_scaled.root
  > > ```
  > {: .solution}
  >
  {: .challenge}


## lxplus Workflow [** OPTIONAL **]
Now that you have seen the few components that go into doing the analysis in total, we need to string them together.  A common computing resource where this can be done at CERN is lxplus. So let's try to write a workflow on lxplus, and for this, we can simply use bash. Start by creating a fresh directory where you will run everything.

This is quite literally all of the steps of the analysis that you would have run by hand, bundled into a single bash script.  __*This is a workflow*__.  Though this is arguably not the most well-preserved or universally useable workflow.

```bash
ssh [your_cern_username]@lxplus.cern.ch
mkdir lxplus_workflow
cd lxplus_workflow
touch run_workflow.sh
```

Now copy into a bash script the following script, being sure to modify the password for the `recasttu` account denoted as `[PASSWORD]` below.  It can be accessed from [this gitlab snippet](https://gitlab.cern.ch/-/snippets/1553) (after 'Password: '), and we don't include it here for security purposes.

~~~bash
# Event Selection
fs setacl -dir $PWD -acl recasttu rlidwka
echo [PASSWORD] | kinit recasttu@CERN.CH
xrdcp root://eosuser.cern.ch//eos/user/r/recasttu/ATLASRecast2021/DAOD_EXOT27.20140688._000071.pool.root.1 DAOD_EXOT27.20140688._000071.pool.root.1
git clone https://gitlab.cern.ch/recast-atlas/tutorial/event-selection.git
cd event-selection/
git submodule init
git submodule update
cd source/
setupATLAS
asetup AnalysisBase,21.2.85,here
ls
cd ../
mkdir build
cd build/
cmake3 ../source/
make -j
source x86_64-centos7-gcc8-opt/setup.sh
AnalysisPayload ../../DAOD_EXOT27.20140688._000071.pool.root.1 output.root 1000


# Post Processing
ls
cd ../../
git clone https://gitlab.cern.ch/recast-atlas/tutorial/post-processing.git
cd post-processing/
python scale_to_lumi_xsec.py -i ../event-selection/build/output.root -o output_scaled.root -p scaled.pdf -c 1 -s 1 -k 1 -f 1 -l 1 -g h_mjj
ls


# Fitting
cd ..
fs setacl -dir $PWD -acl recasttu rlidwka
echo [PASSWORD] | kinit recasttu@CERN.CH
xrdcp root://eosuser.cern.ch//eos/user/r/recasttu/ATLASRecast2021/external_data.root external_data.root
git clone https://gitlab.cern.ch/recast-atlas/tutorial/fitting.git
. /cvmfs/sft.cern.ch/lcg/views/dev4/latest/x86_64-centos7-gcc8-opt/setup.sh
pyhf --version
cd fitting
python run_fit.py --filesig ../post-processing/output_scaled.root --histsig h_mjj  --filebkg ../external_data.root --histbkg background  --filedata ../external_data.root --histdat data --outputfile limits.png --plotfile results.png
~~~

Try running it:

```bash
. run_workflow.sh
```


It should work for the `skimming` and `scaling` steps, but when you get to the `fitting` step, it will fail with a python runtime error
~~~
Fatal Python error: init_import_size: Failed to import the site module
Python runtime state: initialized
Traceback (most recent call last):
  File "/cvmfs/sft-nightlies.cern.ch/lcg/views/dev4/Mon/x86_64-centos7-gcc8-opt/lib/python3.8/site-packages/site.py", line 73, in <module>
    __boot()
...
"/cvmfs/sft.cern.ch/lcg/releases/Python/3.8.6-3199b/x86_64-centos7-gcc8-opt/lib/python3.8/tokenize.py", line 32, in <module>
    import re
  File "/cvmfs/sft.cern.ch/lcg/releases/Python/3.8.6-3199b/x86_64-centos7-gcc8-opt/lib/python3.8/re.py", line 145, in <module>
    class RegexFlag(enum.IntFlag):
AttributeError: module 'enum' has no attribute 'IntFlag'
-bash-4.2$
~~~

The issue here is that the configuration of the environment that occurs when you execute `asetup` set some environment variables which themselves conflicted with the environment that you tried to initialize when you sourced the [LCG view]() with `. /cvmfs/sft.cern.ch/lcg/views/dev4/latest/x86_64-centos7-gcc8-opt/setup.sh`.  This failure point is one of the reasons we want to move to containerizing our analysis stages so that we can have more flexibility and modularity.

> ## Help! I Don't Have Enough Disk Quota!
> If you're getting an error like
> ```bash
> DAOD_EXOT27.20140688._000071.pool.root.1 [2.352GB/2.352GB][100%][==================================================][9.193MB/s]
> Run: [ERROR] Server responded with an error: [3021] disk quota exceeded (destination)
> ```
> Then you may be suffering from lack of disk quota in your `afs` home folder (the default is 2 GB). Fortunately, there is a refreshingly easy fix for this.
>
> Go to your [`afs` settings in the CERN resource portal](https://resources.web.cern.ch/resources/Manage/AFS/Settings.aspx), and click on the button that says 'Increase home quota to 5 GB'. If that's not enough, click on the button that says 'Increase home quota to 10 GB'. Yes, this is a rare occasion where the fix really is that easy :-)
{: .callout}


### With Subshells
We can get around this failure point by using [subshells](https://tldp.org/LDP/abs/html/subshells.html) which allow us to partition our environment setup for each of the steps so that each of the steps is executed in a standalone environment and the output preserved between steps.  The equivalent bash script that will achieve this is shown below.

First, log back into lxplus so we're starting with a fresh environment

```bash
exit
```

```bash
mkdir lxplus_workflow_subshells
cd lxplus_workflow_subshells
touch run_workflow_subshells.sh
```

Paste the following into `run_workflow.sh` (replacing [PASSWORD] with the `recasttu` password) and execute as follows:

```bash
. run_workflow_subshells.sh
```

~~~bash
# run_workflow.sh

# Event Selection
function skimming()
    {
        (
        fs setacl -dir $PWD -acl recasttu rlidwka
        echo [PASSWORD] | kinit recasttu@CERN.CH
        ln -s /eos/user/r/recasttu/ATLASRecast2021/DAOD_EXOT27.20140688._000071.pool.root.1 .
        git clone https://gitlab.cern.ch/recast-atlas/tutorial/event-selection.git
        cd event-selection/
        git submodule init
        git submodule update
        cd source/
        setupATLAS
        asetup AnalysisBase,21.2.85,here
        ls
        cd ../
        mkdir build
        cd build/
        cmake3 ../source/
        make -j
        source x86_64-centos7-gcc8-opt/setup.sh
        # need to get the file from EOS here - hardcoded for now
        AnalysisPayload ../../DAOD_EXOT27.20140688._000071.pool.root.1 output.root 1000
        )
    }

# Post Processing
function scaling()
    {
        (
        pwd
        ls
        git clone https://gitlab.cern.ch/recast-atlas/tutorial/post-processing.git
        cd post-processing/
        pwd
        ls
        ls ../
        python scale_to_lumi_xsec.py -i ../event-selection/build/output.root -o output_scaled.root -p scaled.pdf -c 1 -s 1 -k 1 -f 1 -l 1 -g h_mjj
        ls
        )
    }


# Fitting
function fitting()
    {
        (
        fs setacl -dir $PWD -acl recasttu rlidwka
        echo [PASSWORD] | kinit recasttu@CERN.CH
        xrdcp root://eosuser.cern.ch//eos/user/r/recasttu/ATLASRecast2021/external_data.root external_data.root
        git clone https://gitlab.cern.ch/recast-atlas/tutorial/fitting.git
        . /cvmfs/sft.cern.ch/lcg/views/dev4/latest/x86_64-centos7-gcc8-opt/setup.sh
        pyhf --version
        # need to get the file from EOS here - hardcoded for now
        cd fitting
        python run_fit.py --filesig ../post-processing/output_scaled.root --histsig h_mjj  --filebkg ../external_data.root --histbkg background  --filedata ../external_data.root --histdat data --outputfile limits.png --plotfile results.png
        )
    }


# run each step
skimming
scaling
fitting
~~~

So there we have it, we *can* run our workflow, all of the stages, on lxplus in an interactive way.  It took some bash gymnastics, but was possible.  Next, we will be moving to do this by using standard tools/syntax in a way that reduces these gymnastics.  To do so, we will need to more fully embrace the concept of *containerizing* our work into Docker images.  This will allow us to execute `docker run` commands not only to spin up an interactive container, but execute sequences of commands on the fly.  In essence, we will be rewriting this shell script as a sequence of `docker run` commands, each of which carries with it the unique image environment in which to run, and a volume mounting of the requisite output from previous steps.  However, you will not be seeing any of this explicitly, but rather be configuring this sequence of calls using a new language called [Yadage](https://yadage.readthedocs.io/en/latest/).


> ## GitLab Pipelines as Workflows
> In the GitLab CI tutorial, and earlier in this tutorial, you should have begun developing an expertise in using [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines/).  If you had a tingling spidey sense that you were writing a workflow, then you would be correct!  With the ability to use [artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) to pass files between jobs and save output, Pipelines *are* a type of workflow.  However, there are a few ways that make them not well-suited for analysis preservation in full :
>   - They are intended for use on a single repository and while you can use multiple different images, this is not really the goal of a single Pipeline.
>   - They are not terribly scaleable.  The length of a given job is intended to be short.
> We will see later, however, that we can use GitLab pipelines to run our workflow, once it is written in a standardized way.  With this capability, we can use GitLab CI as an "orchestartor" that runs workflows, although they may be run elsewhere in reality.
{: .callout}



## Why do we need to get fancy and use jazz hands?

If it weren't for the docker containers involved in RECAST, this could conceivably be accomplished with some environment variables and bash scripts that just list out each command that an analyst would type into the terminal while going through the analysis. But when we perform the analysis steps in docker containers, we need a way to codify what needs to happen in which container for each step, and how the output from one step feeds as the input for later steps. This is accomplished through the use of a [declarative](https://en.wikipedia.org/wiki/Declarative_programming) programming language called [`yadage`](https://github.com/yadage/yadage).

Before diving into the gory details of how to actually program with yadage in the next lesson, let's start with a high-level overview of what it is and how it accomplishes the goal of preserving and re-interpreting our analysis.

### Yadage

[Yadage](https://yadage.readthedocs.io/en/latest/) is both:
 * a syntax for describing workflows made up of containerized steps, and
 * an execution engine for actually running these workflows.

We'll get into the syntax part of yadage in the upcoming *intermezzo* lesson, during which you'll get to practice writing a basic helloworld workflow in yadage. But before venturing too far into the forest of syntax, let's first fly overhead and see where this is all going.

In the yadage approach, the workflow is divided into distinct steps, called packaged activities - or "packtivities" - each of which will run inside a docker container. The steps get linked into a workflow by specifying how the output from each such packtivity step feeds in as the input for subsequent steps (this flow of dependencies is not necessarily linear). The yadage workflow engine optimizes the execution of the workflow.  If you want to eat up all the details, then you are encouraged to work through the dedicated [yadage tutorial](https://yadage.github.io/tutorial/) and read the [paper describing the design principles](https://arxiv.org/pdf/1706.01878.pdf)

> ## Declarative Languages
> In this tutorial, we will be using yadage, which itself is an instance of a *declarative* programming language and relies on this concept of "eating" and "spitting".  What goes on under the hood is something that does not concern us when we are programming in a declarative way.  This is opposed to an *imperative* programming language where you write all of the details out that go under the hood (e.g. loops).  There are many other declarative languages (e.g. [serial](https://github.com/reanahub/reana-workflow-engine-serial) and [snakemate](https://snakemake.readthedocs.io/en/stable/)) and you could very well write your workflows using those.  If you find that one of those is easier for you, then go for it! Write your workflow in that language and we will help you translate to yadage to make the conveners happy.
{: .callout}

### Steps (`steps.yml`)
A "step" should be thought of as an atomic unit of a workflow that itself is parameterized in such a way that it can be used over and over, in the same way that your single piece of event loop code is used over and over to process your data and all the various MC samples you may need.  Inside of each step, you will see the sequence of commands that need to be run to successfully perform an execution of whatever utility you are using.  Each step in the workflow is fully described by:
* **Publisher descriptions:** The input data that the step receives from previous step(s) - or from the user if it's the first step in the workflow - and the output that it sends - i.e. "publishes" - to the next step.
* **Process description:** Instructions - typically a bash command or script - for transforming the input data into output to be fed into the next step, or the final result if it's the last step in the workflow
* **Environment description:** The environment in which the instructions need to be executed. This is specified by the image used to start the container in which the step will run.

and these three components are codified with dedicated yadage syntax, as we'll see in the upcoming "Yadage Helloworld" intermezzo.

#### VHbb RECAST Steps

The three steps involved in interpreting our VHbb analysis are as follows:

* **Skimming Step:** This is the step that we're already familiar with, where we take in the signal DAOD, loop through it event-by-event, apply some selections, and output a histogram of the dijet invariant mass variable that we'll want to fit our model to the data. This step will take place in the custom `AnalysisBase` container we recently created for our analysis repo.
* **Scaling Step** This is the bit of post-processing that comes before the actual statistical fit where we scale our signal spectrum to a physically meaningful cross section and luminosity.
* **Interpretation Step:** Here, we pull in the data and background as well as our signal  and do our statistical analysis using pyhf. Out the other side will come our analysis "money plots" that you saw at the start of this lesson.

These steps are summarized in the following illustration:

<img src="../fig/diagram_steps.png" alt="Steps" style="width:800px">

### Workflow (`workflow.yml`)

The workflow description specifies how all the steps "fit together". It accomplishes this by specifying exactly where all the data to be input to each step comes from - whether it be user-provided (i.e. `init` data) or output from a previous step - and how each step will be executed.

Here is an idea of what our workflow should look like:

<img src="../fig/diagram_workflow.png" alt="Workflow" style="width:1200px">

{% include links.md %}
