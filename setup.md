---
title: Setup
---

<h3>Python Version</h3>
Please ensure that you are working with python3 on your computer.  This tutorial is developed
around python3.7 to be precise and issues exist if you live on the bleeding edge and are
using python3.9.  Though much of the tutorial will be performed in docker, running workflows
locally requires this specification.

<h3>Get Your Packages</h3>
Install the necessary python packages on your local machine via [`pip`](https://pypi.org/project/pip/).  If you aren't sure
what `pip` is then consider [reading this intro](https://realpython.com/what-is-pip/) and then running the following :
~~~bash
python -m pip install yadage
python -m pip install recast-atlas
~~~
If you haven't used `pip` before, you may need to install it.

<h3>Get the Credentials</h3>
In a few places, we will be using the `recasttu` [Service Account](https://account.cern.ch/account/Management/NewAccount.aspx).  You will need the credentials for this account, which functions like a normal CERN computing account.  For security purposes, we are not including them in this *public* resource, but you can access them by going to the following gitlab snippet: [gitlab.cern.ch/-/snippets/1553](https://gitlab.cern.ch/-/snippets/1553). **Note:** you'll need to be signed into [gitlab.cern.ch](https://gitlab.cern.ch) with your CERN credentials to view the snippet.

<h3>Install Docker</h3>

Install [docker](https://www.docker.com/) on the machine you intend to use for the tutorial.
We recommend a linux or mac personal computer and *not* lxplus. If
you are wondering why, then this means you need to return to review
the [HSF docker tutorial](https://hsf-training.github.io/hsf-training-docker/index.html).  The directions to do this are well-developed
in the [official docker installation instructions](https://www.docker.com/get-started) but we provide some guidance below.

<h4>Windows</h4>
__It is highly recommended that you DO NOT use Windows__.  Few individuals
use this OS within the HEP community as most tools are designed for Unix-based systems.
If you do have a Windows machine, consider making your computer a dual-boot machine [Link to Directions](https://opensource.com/article/18/5/dual-boot-linux)
Download Docker for Windows [instructions](https://docs.docker.com/docker-for-windows/install/).
Docker Desktop for Windows is the Community Edition (CE) of Docker for Microsoft Windows. To download Docker Desktop for Windows, head to [Docker Hub](https://hub.docker.com/editions/community/docker-ce-desktop-windows)
Please read the relevant information on these pages, it should take no more than 5 minutes.

<h4>Mac OS</h4>
Download Docker for MacOS <a href="https://docs.docker.com/docker-for-mac/install/">instructions</a>.
Docker is a full development platform for creating containerized apps, and Docker Desktop for Mac is the best way to get started with Docker on a Mac. To download Docker Desktop for MacOS, head to <a href="https://hub.docker.com/editions/community/docker-ce-desktop-mac">Docker Hub</a>.
Another common way to install packages on Mac OSX is via the <a href="https://brew.sh/">homebrew</a> package manager.  In the case of docker, you can easily install
docker by setting up homebrew and executing <code>brew cask install docker</code>.


<h4>Linux</h4>
Downloading and installing Docker for Linux may be slightly more difficult. Here are the instructions for two popular Linux distributions - (<a href="https://docs.docker.com/install/linux/docker-ce/CentOS/">CentOS</a>, <a href="https://docs.docker.com/install/linux/docker-ce/ubuntu/">Ubuntu</a>)
Instructions for other Linux distributions can be found on the Docker docs pages. <font color="red">Be sure to read the Docker documentation on post-installation steps for Linux and managing Docker
as a </font> <a href="https://docs.docker.com/install/linux/linux-postinstall/">non-root user</a>. <font color="red">This will allow you to edit files when you have started up the Docker container from your image. </font>


<h3>Get the Images</h3>

Please do the following docker pulls beforehand to save time during the tutorial:
  ~~~bash
  docker pull atlas/analysisbase:21.2.85-centos7  # yeah yeah we know, this is an old release, cool your horses, there are people still doing Run1 analyses, so old releases can be great too, and its good enough for what we need
  docker pull matthewfeickert/intro-to-docker
  docker pull debian:buster
  docker pull yadage/yadage
  docker pull yadage/tutorial-messagewriter
  docker pull yadage/tutorial-uppermaker
  ~~~

After logging into the gitlab registry with your CERN username and password, also pull the image we'll use during the tutorial for the latter stages of our analysis. We'll
be pretending that these are packages written by other people and you are just going to be using them :
  ~~~bash
  docker login gitlab-registry.cern.ch
  docker pull gitlab-registry.cern.ch/recast-atlas/tutorial/event-selection:master-0a6b4582
  docker pull gitlab-registry.cern.ch/recast-atlas/tutorial/post-processing:master-5173e8ae
  docker pull gitlab-registry.cern.ch/recast-atlas/tutorial/fitting:master-3277bf68
  ~~~

<h3>Get the Signal Sample</h3>
Get one file from the VHbb signal sample that we'll be running over with our analysis code from this container :
```
mc16_13TeV.345055.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r10724_p4320
```

The rucio download can be done on lxplus as follows (assuming you have VOMS grid credentials):

```bash
setupATLAS
lsetup rucio
voms-proxy-init -voms atlas
rucio get --nrandom 1 mc16_13TeV:mc16_13TeV.345055.PowhegPythia8EvtGen_NNPDF3_AZNLO_ZH125J_MINLO_llbb_VpT.deriv.DAOD_EXOT27.e5706_s3126_r10724_p4320
```

If you don't know or want to use [rucio](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/RucioClientsHowTo) to get this, then there is [one available on EOS at this link](https://cernbox.cern.ch/index.php/s/StRgQkdYUS9FMnV).  After downloading, just keep it somewhere where you'll be able to find it again during the tutorial.

<h3>Get a token for the CERN REANA cluster</h3>

**NOTE: You can skip this setup step if you know for sure that you don't plan on doing the bonus episode 'Running your Workflow on REANA'**

To use the REANA cluster running at CERN (the one with all the resources for big input files and long run times and all that), you'll need to request an access token if you don't yet have one. In case you don't have a token yet, you'll be prompted to request one the first time you open the user interface for the cluster in your browser. Go to [reana.cern.ch](https://reana.cern.ch) in your browser.

If after logging in with your personal CERN credentials you see a page like this:

<img src="fig/request_reana_token_page.png" alt="REANA request token page" style="width:700px">

then click on the `Request token` button and you should be hooked up with a shiny new access token within a CERN working day or so. Once you have a token, you can go to your profile at [reana.cern.ch/profile](https://reana.cern.ch/profile) to access the bash commands needed to set the environment variables `REANA_SERVER_URL` and `REANA_ACCESS_TOKEN` that the `reana-client` uses to access the CERN REANA cluster:

<img src="fig/reana_profile.png" alt="REANA profile" style="width:700px">

{% include links.md %}
